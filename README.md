## Storing super cookies in browser history

This is a demonstration of how the :visited pseudo class of css and fake CAPTCHAs can be used to store supercookies.  
[Demo Page](https://hxxpxxp.gitlab.io/visited_css_history_cookie)  

### How it works

When no browser cookie is present user is asked to solve a CAPTCHA.  
Visiblity of characters in CAPTCHA is dependent on users history.  
If user is visiting for the first time then a new userID is generated and stored in user's history.  
If the user has visited before but the cookies are cleared then with the help of user solving the CAPTCHA, that previous userID is revealed and again stored in cookies.  
So basically to write the cookie webpages are visited and to read back the cookie user and :visited css pseudo class is used.  

#### This concept has previously been shown to be able to leak user's history:
[How CSS can leak your browser history](https://josephpetitti.com/blog/how-css-can-leak-your-browser-history)  
[A POC tricking the end user of a captcha to reveal his browser history](https://github.com/frantzmiccoli/visited-captcha-history)  

